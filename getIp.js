var Horseman = require("node-horseman");
var mongoClient = require('mongodb').MongoClient;
var args = process.argv.slice(2),
	port = args[0];

var getIP = function(timeout) {

	console.log(
		"Current timeout: " + timeout
	);

	if ( timeout > 16000 ) {
		console.log(
			"Timeout is OVER16000. It is too powerful! Abandon operation!"
		);
		process.exit();
	}

	var proxy = 'localhost:' + port;

	var horseman = new Horseman({
		timeout: timeout,
		proxy: proxy,
		proxyType : 'socks5'
	});

	horseman
		.open('https://api.ipify.org/')
		.userAgent('Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36')
		.html('pre')
		.then(function(text) {
			console.log(
				text
			);
			mongoClient.connect('mongodb://localhost:27017/admin', function (err, db) {
				db.collection('ips', function( err, collection ) {
					collection.insert({ 'ip': text }, {}, function(err, result) {
						if ( err ) {
							console.log( err );
						}

						console.log(
							"Here is the result: "
						);
						console.log( result );
						console.log( "Closing" );

						horseman.close();
						process.exit();
					});

				});
			});
		}).catch(function( ) {
			console.log( "Failed. Increase timeout by " + timeout );
			timeout += timeout;
			getIP(timeout);
		});
};

var timeout = 1000;

getIP(timeout);